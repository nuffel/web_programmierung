from django.contrib import admin
from .models import shopping_list, shop_entry, shopping_list_entry

admin.site.register(shopping_list_entry)
admin.site.register(shop_entry)
admin.site.register(shopping_list)


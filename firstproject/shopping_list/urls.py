from django.urls import path
from . import views

app_name = "shopping_list"

urlpatterns = [
        path('', views.index, name='index'),
        path('delete_list' , views.delete_list, name='delete_list'),
        path('delete_shop' , views.delete_shop, name='delete_shop'),
        path('delete_shopping_list_entry' , views.delete_shopping_list_entry, name='delete_shopping_list_entry'),
        path('new_list' , views.new_list, name='new_list'),
        path('new_shop' , views.new_shop, name='new_shop'),
        path('new_list_entry' , views.new_list_entry, name='new_list_entry'),
        path('list/<int:list_id>', views.list, name='list'),
        path('shop/<int:shop_id>', views.shop, name='shop')
        ]

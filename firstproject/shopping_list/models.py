from django.db import models

class shopping_list(models.Model):
   shopping_list_name = models.CharField(max_length=200) 

   def __str__(self):
       return f"{self.shopping_list_name}"


class shop_entry(models.Model):
    shop_name = models.CharField(max_length=200)
    shop_open_time = models.CharField(max_length=200)
    
    def __str__(self):
        return f"{self.shop_name}"

class shopping_list_entry(models.Model):
    
    shopping_list_entry_name = models.CharField(max_length=200)
    shopping_list_entry_price = models.FloatField(blank=True)
    shopping_list_entry_amount = models.IntegerField(blank=True)
    shopping_list_entry_done = models.BooleanField(default=False)
    shopping_list_entry_shop = models.ForeignKey(shop_entry, null=True, on_delete=models.SET_NULL)
    shopping_list_entry_list = models.ForeignKey(shopping_list, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.shopping_list_entry_name}"


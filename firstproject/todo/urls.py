from django.urls import path
from . import views

app_name = "todo"

urlpatterns = [
        path('', views.index, name='index'),
        path('new_entry', views.new_entry, name="new_entry"),
        path('delete_entry', views.delete_entry, name="delete_entry"),
        ]

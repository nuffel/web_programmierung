# Generated by Django 4.0.3 on 2022-04-13 21:11

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('todo', '0006_alter_todo_entry_todo_progress'),
    ]

    operations = [
        migrations.AlterField(
            model_name='todo_entry',
            name='todo_date',
            field=models.DateField(default=django.utils.timezone.now),
        ),
    ]

from local_credentials import * # imports client_id and client_secret
import os
import spotipy
from django.shortcuts import render

cache_file = './spotify_test/spotify_cache/user'

def index(request):
    logged_in = False

    if os.path.isfile(cache_file):
        logged_in = True

    context = { 'logged_in':logged_in }
    return render(request, 'spotify_test/index.html', context)

def auth(request):
    global cache_file
    redirect_uri = 'http://127.0.0.1:8080'

    if not os.path.exists(os.path.dirname(cache_file)):
        os.makedirs(os.path.dirname(cache_file))

    cache_handler = spotipy.cache_handler.CacheFileHandler(cache_path=cache_file)
    auth_manager = spotipy.oauth2.SpotifyOAuth(client_id=client_id, 
                                               client_secret=client_secret, 
                                               redirect_uri=redirect_uri, 
                                               scope='user-read-recently-played', 
                                               cache_handler=cache_handler, 
                                               show_dialog=True)

    # username = request.POST['username']

    sp = spotipy.Spotify(auth_manager=auth_manager)
    print(sp.current_user_recently_played())
    data = []
    for s in sp.current_user_recently_played()['items']:
        track_name = s['track']['name']
        artist_name = s['track']['album']['artists'][0]['name']
        played_ts = s['played_at']
        print()
        print(f"{track_name} by {artist_name} ({played_ts})")
        data.append({'track':track_name, 'artist': artist_name, 'played_ts': played_ts})

    print(data)

    return render(request, 'spotify_test/auth.html', {'data':data})

def logout(request):
    os.remove(cache_file)
    return render(request, 'spotify_test/index.html')

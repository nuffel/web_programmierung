from django.apps import AppConfig


class SpotifyTestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'spotify_test'

from django.urls import path
from . import views

app_name = "spotify_test"

urlpatterns = [
        path('', views.index, name='index'),
        path('auth', views.auth, name='auth'),
        path('logout', views.logout, name='logout'),
        path('index', views.index, name='index'),
        ]

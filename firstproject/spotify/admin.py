from django.contrib import admin
from .models import Track, RecentTrack, Artist, Genre

admin.site.register(Track)
admin.site.register(RecentTrack)
admin.site.register(Artist)
admin.site.register(Genre)


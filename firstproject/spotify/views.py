from django.shortcuts import render, redirect, get_object_or_404
from . import spotify, queries
from .models import RecentTrack, Track, Artist, Genre

spc = spotify.SpotifyClient()

def index(request):
    if not spc.is_logged_in():
        return spc.get_connect_redirect()

    spc.update_recently_played() # Updated recently played
    
    recently_played = queries.get_recently_played_list(15)
    sorted_tracks, sorted_artists, sorted_genres = queries.get_weighted_sorted_lists()
    
    page = {
            'title' : "Spotify",
            'subtitle' : "Your personalized Spotify Dashboard",
            }

    context = {
               'page' : page,
               'recently_played': recently_played,
               'sorted_tracks': sorted_tracks[:10],
               'sorted_artists': sorted_artists[:10],
               'sorted_genres': sorted_genres[:10],
              }

    
    return render(request, 'spotify/index.html', context)


def track(request, id):
    track = get_object_or_404(Track, pk=id)
    track_info = queries.get_track_info(spc, track)    
    
    page = {
            'title' : track.name,
            'subtitle' : "Track",
            }

    context = {
            'page': page,
            'track' : track,
            'artists': track_info['artists'],
            'album': track_info['album'],
            'genres': track_info['genres'],
            'cover_url': track_info['cover_url'],
            'open_in_spotify_url': track_info['open_in_spotify_url'],
            'recently_played': track_info['recently_played'],
            }

    return render(request, 'spotify/track.html', context)


def artist(request, id):
    artist = get_object_or_404(Artist, pk=id)
    artist_info = queries.get_artist_info(spc, artist)

    page = {
            'title' : artist.name,
            'subtitle' : "Artist",
            }
    
    context = {
            'page': page,
            'artist': artist,
            'genres': artist_info['genres'],
            'recently_played': artist_info['recently_played'],
            'image_url': artist_info['image_url'],
            'open_in_spotify_url': artist_info['open_in_spotify_url'],
            }

    return render(request, 'spotify/artist.html', context)


def genre(request, id):
    genre = get_object_or_404(Genre, pk=id)
    genre_info = queries.get_genre_info(genre)
    
    page = {
            'title' : genre.name,
            'subtitle' : "Genre" 
            }

    context ={
            'page': page,             
            'genre': genre,
            'genre_artists': genre_info['genre_artists'],
            'recently_played': genre_info['recently_played'][:15],
            }

    return render(request, 'spotify/genre.html', context)


def change_account(request):
    spc.logout()
    return redirect(index)


def spotify_callback(request):
    if 'code' in request.GET:
        spc.spotify_authorization_code = request.GET['code']
    spc.get_token() # create and save the first token
    return redirect(index)

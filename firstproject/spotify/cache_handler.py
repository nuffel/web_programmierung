import os
from datetime import datetime
import json
from django.conf import settings
from .utilities import print_log

class CacheHandler():
    def __init__(self):
        self.cache_dir = os.path.join(settings.BASE_DIR, "spotify", "cache")
        self.user_cache = os.path.join(self.cache_dir, "user_cache.json")
        
        if not os.path.isdir(self.cache_dir):
            os.mkdir(self.cache_dir)


    def set_cache_token(self, data, token_expires_at, refresh_token):
        data.update({'token_expires_at': token_expires_at.strftime('%Y-%m-%dT%H:%M:%S.%f')}) # json cant process datetime object
        data.update({'refresh_token': refresh_token})
        with open(self.user_cache, 'w') as file:
            json.dump(data, file)
            print_log("SAVED TOKEN TO FILE", data)


    def get_cache_token(self, access_token, refresh_token, token_expires_at):
        data ={'access_token':access_token, 'refresh_token': refresh_token, 'token_expires_at': token_expires_at}
        if os.path.isfile(self.user_cache):
            with open(self.user_cache) as file:
                data = json.load(file)
                data.update({'token_expires_at': datetime.strptime(data['token_expires_at'], "%Y-%m-%dT%H:%M:%S.%f")}) # reconvert from str to datetime obj
                print_log("GOT CACHED TOKEN", data)
       
        return data['access_token'], data['refresh_token'], data['token_expires_at']

    def delete_cache(self):
        os.remove(self.user_cache)


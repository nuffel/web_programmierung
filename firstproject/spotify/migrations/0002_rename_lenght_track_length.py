# Generated by Django 4.0.3 on 2022-04-24 15:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spotify', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='track',
            old_name='lenght',
            new_name='length',
        ),
    ]

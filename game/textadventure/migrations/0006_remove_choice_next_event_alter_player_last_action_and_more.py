# Generated by Django 4.0.3 on 2022-06-05 17:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('textadventure', '0005_remove_player_stories_player_finished_events_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='choice',
            name='next_event',
        ),
        migrations.AlterField(
            model_name='player',
            name='last_action',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='player',
            name='story_text',
            field=models.TextField(default=''),
        ),
    ]

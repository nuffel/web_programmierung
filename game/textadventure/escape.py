from .models import Choice, Event


def get_hearts(player):
    hearts = {
            'full': range(0, int(player.actions / 2)),
            'half': range(0, player.actions % 2),
            'empty': range(0, int((player.start_actions - player.actions)/2))
            }
    return hearts


def get_available_choices(player, events):
    items = player.items.all()
    choices = []
    for event in events:
        all_choices = Choice.objects.filter(event=event.id)
        # Remove choices if the player lacks the required item or
        # the choice was already used or discarde
        for choice in all_choices:
            if (choice not in player.chosen_choices.all() and
                    choice not in player.discarded_choices.all() and
                    (not choice.required_item or
                     choice.required_item in items.all())):
                choices.append(choice)

    return choices


def get_current_events(player, station):
    all_events = Event.objects.filter(station=station)

    current_events = []
    for event in all_events:
        if (event not in player.finished_events.all() and
                (not event.required_event or
                 event.required_event in player.finished_events.all())):
            current_events.append(event)

    if len(current_events) < 1:
        return "no_event"
    else:
        return current_events

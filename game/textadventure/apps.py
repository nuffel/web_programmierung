from django.apps import AppConfig


class TextadventureConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'textadventure'

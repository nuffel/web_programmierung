from django.urls import path
from . import views

urlpatterns = [
        path('', views.index, name='index'),
        path('book/<id>', views.book, name='book'),
        path('bibliography', views.bibliography, name='bibliography'),
        path('new_book', views.new_book, name='new_book'),
        path('edit_book/<id>', views.edit_book, name='edit_book'),
        path('add_book_to_library', views.add_book_to_library, name='add_book_to_library'),
        path('update_book_in_library/', views.update_book_in_library, name='update_book_in_library'),
        path('delete_book/<id>', views.delete_book, name='delete_book'),
        ]

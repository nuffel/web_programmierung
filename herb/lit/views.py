from django.shortcuts import render, redirect, get_object_or_404

from .models import Book
from . import openlibrary
from . import lit

# Used to store bibliograhy (at least in the current session)
# to keep information while reloading
bibliography_settings = {
    'checked_books': [],
    'cite': 'apa',
    'selection': 'selected',
}


def index(request):
    books = Book.objects.all().order_by('title')

    # Used to not render the list/buttons if there were no books added yet
    empty = True
    if len(books) > 0:
        empty = False

    context = {
        'pagetitle': 'LIT',
        'pagesubtitle': 'Your personal literatur management system',
        'books': books,
        'empty': empty,
    }

    return render(request, 'lit/index.html', context)


def book(request, id):
    book = get_object_or_404(Book, pk=id)
    context = {
        'pagetitle': book.title,
        'pagesubtitle': 'HOME',
        'book': book,
        'citation_apa': lit.cite_book("apa", book),
        'citation_ieee': lit.cite_book("ieee", book),
        'citation_mla': lit.cite_book("mla", book),
    }

    # try to get cover and link if an ISBN is given
    if book.isbn != "":
        links = openlibrary.get_openlibrary_links(book.isbn)
        if links:
            context.update({
                'links': links,
                'openlibrary': True,
            })

    return render(request, 'lit/book.html', context)


def new_book(request):
    context = {
        'pagetitle': 'Add New Book',
        'pagesubtitle': 'HOME',
    }

    # add information to the template if there is an isbn given
    if 'isbn' in request.POST:
        isbn = request.POST['isbn']
        book = openlibrary.get_by_isbn(isbn)
        context.update({'book': book})

    return render(request, 'lit/book_attributes.html', context)


def edit_book(request, id):
    book = lit.get_book_info_by_id(id)
    context = {
        'pagetitle': 'Edit Book',
        'pagesubtitle': 'HOME',
        'book': book
        }
    return render(request, 'lit/book_attributes.html', context)


def bibliography(request):
    global bibliography_settings

    # update information if there was a POST
    if request.POST:
        bibliography_settings = lit.update_bibliography_settings(
            request.POST, bibliography_settings)

    selected_books = lit.get_selected_books(bibliography_settings)

    # if no books are selected do nothing
    if not selected_books:
        return redirect(index)

    bibliography = lit.create_bibliography(
        bibliography_settings['cite'], selected_books)

    context = {
        'pagetitle': 'Bibliography',
        'pagesubtitle': 'HOME',
        'bibliography': bibliography,
        'cite': bibliography_settings['cite']
    }

    # print(bibliography)
    return render(request, 'lit/bibliography.html', context)


def add_book_to_library(request):
    if request.POST:
        lit.add_book(request.POST)
    return redirect(index)


def update_book_in_library(request):
    if request.POST:
        id = request.POST['id']
        lit.update_book(request.POST, id)
        return redirect(book, id)
    else:
        return redirect(index)


def delete_book(request, id):
    book = get_object_or_404(Book, id=id)
    book.delete()
    return redirect(index)

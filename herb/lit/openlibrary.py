import requests
import json
import re

URL = "https://openlibrary.org"
COVERS_URL = "https://covers.openlibrary.org/b/isbn"


def get_openlibrary_links(isbn):
    # check if the isbn can be found on openlibrary
    book_url = f"{URL}/isbn/{isbn}"
    response = requests.get(f"{book_url}.json")
    if response.status_code != 404:
        return {'site': f"{URL}/isbn/{isbn}",
                'cover': f"{COVERS_URL}/{isbn}-L.jpg"}


def get_by_isbn(isbn):
    book_url = f"{URL}/isbn/{isbn}"
    response = requests.get(f"{book_url}.json")
    data = json.loads(response.text)

    if 'error' in data:  # api returns error if the isbn cant be found
        return None

    # print("FOUND NEW BOOK")
    # print(json.dumps(data, indent=4))

    # Get the author names as the isbn book call only returns a key
    main_author = ''
    additional_authors = []
    if 'authors' in data:
        for i, author in enumerate(data['authors']):
            author = get_author(author['key'])
            if i == 0:
                main_author = author
            else:
                additional_authors.append(author)

    # default to isbn 13 and use isbn 10 as a fallback
    isbn = ""
    if 'isbn_13' in data:
        isbn = data['isbn_13'][0]
    elif 'isbn_10' in data:
        isbn = data['isbn_10'][0]

    # list all publishers as one string
    publishers = ""
    if 'publishers' in data:
        for i, publisher in enumerate(data['publishers']):
            if i != 0:  # create comma seperated list
                publishers += ", "
            publishers += publisher

    year = ""
    if 'publish_date' in data:
        year = parse_year(data['publish_date'])

    edition = ""
    if 'edition_name' in data:
        edition = data['edition_name']

    # openlibrary returns a list, defaults to the first given place
    place = ""
    if 'publish_places' in data:
        place = data['publish_places'][0]

    book = {
            'author': main_author,
            'additional_authors': additional_authors,
            'title': data['title'],
            'edition': edition,
            'year': f"{year}",
            'publisher': publishers,
            'place': place,
            'isbn': isbn,
            'doi': '',
            }

    return book


# parses date string and returns the first value which contains a valid year
def parse_year(date):
    results = re.findall('\d{4}', date)
    for result in results:
        year = int(result)
        # future proof for the next 478 years and exclude the bible
        if year > 1440 and year < 2500:
            return year


# get author name form openlibrary and parse initials/surname/firstnames
def get_author(author_key):
    response = requests.get(f"{URL}/{author_key}.json")
    data = json.loads(response.text)
    author_name = data['name']

    # basic way to get initials and surname
    initials = ""
    # split name at whitespaces (after replacing '.'
    # to keep already initialized names)
    for s in author_name.replace('.', ' ').split():
        # make initials if its not the last part of the name
        if author_name.endswith(s):
            return {
                    'full_name': author_name,
                    'surname': s,
                    # remove surname -> firstnames
                    'firstnames': author_name[:-len(s)].strip(),
                    'initials': initials}
        initials += f"{s[0]}."

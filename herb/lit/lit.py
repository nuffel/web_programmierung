import re
from django.shortcuts import get_object_or_404

from .models import Book, Author


def add_book(data):

    author = Author.objects.get_or_create(
        surname=data['author_surname'],
        initials=data['author_initials'],
        firstnames=data['author_firstnames'])

    book = Book(
        title=data['title'],
        author=author[0],
        year=data['year'],
        edition=data['edition'],
        publisher=data['publisher'],
        place=data['place'],
        doi=data['doi'],
    )

    # as isbn is an integer field, sanitize the input and remove all non digits
    isbn = re.sub(r'[\D]', '', data['isbn'])
    if isbn != '':
        book.isbn = isbn

    book.save()

    surname_list = data.getlist('additional_authors_surname')
    initials_list = data.getlist('additional_authors_initials')
    firstnames_list = data.getlist('additional_authors_firstnames')

    for i in range(len(surname_list)):
        # Only add additional authors if their information is complete
        if firstnames_list[i] != "" and surname_list[i] != "" and initials_list[i] != "":
            author = Author.objects.get_or_create(
                surname=surname_list[i],
                initials=initials_list[i],
                firstnames=firstnames_list[i])
            book.additional_authors.add(author[0].id)

    print(f"ADDED NEW BOOK: {book}")


def update_book(data, id):
    book = get_object_or_404(Book, id=id)

    author = Author.objects.get_or_create(
        surname=data['author_surname'],
        initials=data['author_initials'],
        firstnames=data['author_firstnames'])

    book.title = data['title']
    book.author = author[0]
    book.year = data['year']
    book.edition = data['edition']
    book.publisher = data['publisher']
    book.place = data['place']
    book.doi = data['doi']

    # as isbn is an integer field, sanitize the input and remove all non digits
    isbn = re.sub(r'[\D]', '', data['isbn'])
    if isbn != '':
        book.isbn = isbn
    else:
        # used to overwrite isbn if there was one before
        book.isbn = None

    # Clear first to allow removing authors
    book.additional_authors.clear()

    surname_list = data.getlist('additional_authors_surname')
    initials_list = data.getlist('additional_authors_initials')
    firstnames_list = data.getlist('additional_authors_firstnames')

    for i in range(len(surname_list)):
        if firstnames_list[i] != "" and surname_list[i] != "" and initials_list[i] != "":
            author = Author.objects.get_or_create(
                surname=surname_list[i],
                initials=initials_list[i],
                firstnames=firstnames_list[i])
            book.additional_authors.add(author[0].id)

    book.save()


def cite_book(style, book):
    citation = ""
    main_author = book.author
    additional_authors = book.additional_authors.all()

    # APA
    if style.lower() == "apa":
        citation += f"{main_author.surname}, {main_author.initials}"
        if len(additional_authors) <= 2:
            citation += ', '

        for i, author in enumerate(additional_authors):
            # if there are more than 3 authors, list the first one + "et al."
            if len(additional_authors) > 2:
                citation += " et al., "
                break

            # add & to the last of multiple authors
            if i == len(additional_authors) - 1:
                citation += f" & {author.surname}, {author.initials} "
            else:
                citation += f" {author.surname}, {author.initials}, "

        edition = ""
        if book.edition:
            edition = f" ({book.edition})"

        citation += f"({book.year}). {book.title}{edition}. {book.publisher}"
        return citation

    # IEEE
    if style.lower() == "ieee":

        citation += f"{main_author.initials} {main_author.surname}"
        if len(additional_authors) <= 2:
            citation += ', '

        for i, author in enumerate(additional_authors):
            # if there are more than 3 authors, list the first one + "et al."
            if len(additional_authors) > 2:
                citation += " et al., "
                break

            # add 'and' to the last of multiple authors
            if i == len(additional_authors) - 1:
                citation += "and "
            citation += f"{author.initials} {author.surname}, "

        citation += f"{book.title}. {book.place}: {book.publisher}, {book.year}"
        return citation

    # MLA
    if style.lower() == "mla":

        citation += f"{main_author.surname}, {main_author.firstnames}"

        if len(additional_authors) > 1:
            citation += " et al., "
        elif len(additional_authors) > 0:
            citation += f" and {additional_authors[0].firstnames} {additional_authors[0].surname}. "
        else:
            citation += '. '

        edition = ""
        if book.edition:
            edition = f" {book.edition},"

        citation += f"{book.title}.{edition} {book.place}, {book.publisher}, {book.year}"
        return citation


def create_bibliography(style, books):
    bibliography = []
    books = books.order_by('author__surname')

    for book in books:
        bibliography.append(cite_book(style, book))

    return bibliography


def update_bibliography_settings(data, bibliography_settings):
    if 'selection' in data:
        bibliography_settings['selection'] = data['selection']

    bibliography_settings['cite'] = data['citation']

    # Avoid errors with empty selections
    if 'checked_books' in data:
        bibliography_settings['checked_books'] = data.getlist(
            'checked_books')
    else:
        bibliography_settings['checked_books'] = []

    return bibliography_settings


def get_selected_books(current_bibliography):
    if current_bibliography['selection'] == 'all':
        selected_books = Book.objects.all()
    elif current_bibliography['selection'] == 'selected':
        selected_books = Book.objects.filter(
            id__in=current_bibliography['checked_books'])
    elif current_bibliography['selection'] == 'not_selected':
        selected_books = Book.objects.exclude(
            id__in=current_bibliography['checked_books'])
    else:
        selected_books = Book.objects.none()

    return selected_books


def get_book_info_by_id(id):
    book = get_object_or_404(Book, id=id)

    additional_authors = []
    for i, author in enumerate(book.additional_authors.all()):
        additional_authors.append(author)

    book_info = {
        'id': id,
        'author': book.author,
        'additional_authors': additional_authors,
        'title': book.title,
        'year': book.year,
        'publisher': book.publisher,
        'edition': book.edition,
        'place': book.place,
        'isbn': book.isbn,
    }

    return book_info
